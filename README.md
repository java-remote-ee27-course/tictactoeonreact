# React demo

A React tictactoe demo practice app on Next.js.
It has:
* browsing in the game history
* sorting history asc / desc
* text to say next players and winners


(see also an initial example: https://react.dev/learn/tutorial-tic-tac-toe) 
## Version 4 (added: highlight the three squares that caused the win ):
<img src = "./assets/demo6.png" alt="page-demo" />
<img src = "./assets/demo7.png" alt="page-demo" />

## Version 3 (added sorting of the history data asc/desc):
<img src = "./assets/demo4.png" alt="page-demo" />
<img src = "./assets/demo5.png" alt="page-demo" />

## Version 2 (added browsing in game history):
<img src = "./assets/demo2.png" alt="page-demo" />
<img src = "./assets/demo3.png" alt="page-demo" />

## Version 1 (initial version):
<img src = "./assets/demo1.png" alt="page-demo" />

