
'use client';   /** for client side interactivity */

import {
    MDBCard,
    MDBCardBody,
} from 'mdb-react-ui-kit';

/** Tic-tac-toe game initial idea: https://react.dev/learn/tutorial-tic-tac-toe */
/** Sorting example: https://www.altcademy.com/blog/how-to-sort-the-table-in-ascending-order-in-reactjs/ */

/** Create square */
function Square({ value, onSquareClick, styles }) {

    return (
        <button className="square" onClick={onSquareClick} style={styles}>
            {value}
        </button>
    );
}

/** Create the 3x3 board and X, O logic */
let count = 0;
export default function Board({ xIsNext, squares, onPlay }) {
    let winningSquares = [];//13.04

    function handleClick(i) {
        //if square already has X or O, return
        //if winner has found, return
         
        if (squares[i] || winner) {
            return;
        }
        const nextSquares = squares.slice();

        if (xIsNext) {
            nextSquares[i] = "X";
        } else {
            nextSquares[i] = "O";
        }
        //a copy of the current state of 
        //the squares array is given to prop onPlay:
        onPlay(nextSquares);
        count++;
    }

    /** Calculate winner */
    const winner = calculateWinner(squares);
    let status;

    if (winner) {
        status = "The winner is: " + calculateWinner(squares);
    } else if (count >= 9) {
        status = "The game ended with a draw.";
    }
    else {
        status = "The next player is: " + (xIsNext ? "X" : "O");
    }

    /** Generate the rows with squares for tictactoe game */
    const rows = [...Array(3).keys()];
    const cols = 3;    
    let gameArray = rows.map((row) => {
        let squaresArray = [...Array(cols).keys()].map((col) => {
            let index = row * cols + col;
            const style = {};

            /** Add conditional styling to the winner: */
            if(winner){
                if (index === winningSquares[0] || index === winningSquares[1] || index === winningSquares[2]) {
                    style.backgroundColor = 'red';
                } 
            }
            return (<Square 
                key={index}
                value={squares[index]}
                onSquareClick={() => handleClick(index)}
                styles = {style}
            />
            );
        });
        
        return (
            <div className="board-row" key={row}>
                {squaresArray}
            </div>
        )
    });

    /** return tictactoe game */
    return (
        <MDBCard shadow="5">
            <div className="card-header">
                <h4>Play!</h4>
            </div>
            <MDBCardBody>
                <div className="card-image">
                    <img src='/assets/pixabay.png' fluid="true" alt='cardimage' />
                    <a>
                        <div className='mask' style={{ backgroundColor: 'rgba(251, 251, 251, 0.15)' }}></div>
                    </a>
                </div>
                <div className="game-container">
                    {gameArray}
                </div>
            </MDBCardBody>

            <div className="card-footer">
                <div className="status">{status}</div>
            </div>
        </MDBCard>
    );


    function calculateWinner(squares) {
        const lines = [
            [0, 1, 2],
            [3, 4, 5],
            [6, 7, 8],
            [0, 3, 6],
            [1, 4, 7],
            [2, 5, 8],
            [0, 4, 8],
            [2, 4, 6]
        ];

        for (let i = 0; i < lines.length; i++) {
            const [a, b, c] = lines[i];
            if (squares[a] && squares[a] === squares[b] && squares[a] === squares[c]) {
                /** push squares to winningSquares for the later styling */
                winningSquares.push(a, b, c);
                return squares[a];
            }
        }
        return null;
    }


}
