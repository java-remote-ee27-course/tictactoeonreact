'use client';

import Board from "./Board";
import { useState } from 'react';
import { MDBBtn } from 'mdb-react-ui-kit';
import { MDBTable, MDBTableHead, MDBTableBody } from 'mdb-react-ui-kit';

export default function Game() {
    const [history, setHistory] = useState([Array(9).fill(null)]);
    const [sortConfig, setSortConfig] = useState({ key: 'GAME HISTORY &nbsp;&#8645;', direction: 'ascending' });
    const [currentMove, setCurrentMove] = useState(0);
    const xIsNext = currentMove % 2 === 0;
    const currentSquares = history[currentMove];


    /** Keep the history up to the point where you are 
    (i.e. if you go back in history) */
    function handlePlay(nextSquares) {
        const nextHistory = [...history.slice(0, currentMove + 1), nextSquares];
        setHistory(nextHistory);
        setCurrentMove(nextHistory.length - 1);
    }


    function goToMove(nextMove) {
        setCurrentMove(nextMove);
    }

    /** 
     * History output: buttons to jump to previous steps
     * Current move is displayed as a div (not a button)
     */

    const moves = history.map((squares, move) => {
        let description;
        if (move === currentMove) {
            description = "You are now in a move " + move;
            return (
                <tr key={move}>
                    <td className="current-move-col">{description}</td>
                </tr>
            );
        }
        else if (move > 0) {
            description = "Go to move " + move;

        } else {
            description = "Go to game start";
        }
        return (
            <tr key={move}>
                <td><MDBBtn key={move} color="primary" onClick={() => goToMove(move)}>{description}</MDBBtn></td>
            </tr>
        )
    });

    /** check if the key is the same as the current sort (sortConfig) key */
    const requestSort = (key) => {
        let direction = "ascending";
        if (sortConfig.key === key && sortConfig.direction === "ascending") {
            direction = "descending";
        }
        setSortConfig({ key, direction });
    }

    /** sort sortedMoves based on sortConfig */
    let sortedMoves = [...moves.slice()];
    if (sortConfig.direction === 'descending') {
        sortedMoves.reverse();
    }


    return (
        <>
            <div className="app-container">
                <h1 className="mt-5 mb-5">Tic-tac-toe</h1>
                <div className="board">
                    <Board xIsNext={xIsNext} squares={currentSquares} onPlay={handlePlay} />
                </div>
            </div>
            <div className="table-container">
                <MDBTable className="table mt-5 mb-5 history-table" >
                    <MDBTableHead>
                        <tr className='mt-1'>
                            <th className="bg-dark text-dark bg-opacity-10 game-history-th" onClick={() => requestSort('GAME HISTORY &nbsp;&#8645;')}>
                                GAME HISTORY &nbsp;&#8645;
                            </th>
                        </tr>
                    </MDBTableHead>
                    <MDBTableBody>
                        {sortedMoves}
                    </MDBTableBody>
                </MDBTable>
            </div>
        </>
    );
}


